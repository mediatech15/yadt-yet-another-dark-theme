# Change Log

## 1.0.0

- Initial release

## 1.1.0

- Reduced string harshness
- Fixed debug statusbar readability

## 1.2.0

- Added better XML colorization

## 1.3.0

- Fixed publishing