# YADT (Yet Another Dark Theme)

Another dark theme in the ocen of dark themes. My personal take. 

* This was looked at from a completionist point of view. Most keys modified
* This was looked at across 20ish test files, and some personal repos
* UI and code theme alignment
* Dark but not amoled dark
* Bright where needed muted where not
* Grays are pure and not blue/red/green tinted
* Native bracket colorization (only 3 before repeat as to not be the rainbow)

## Screenshots

### UI

![](images/ui.png)

### Typescript

![](images/ts.png)

### Python

![](images/python.png)

### C/C++

![](images/c.png)