import * as path from 'path';

// Example -----------------------------

const myGreeter = new Greeter("hello, world");
myGreeter.greeting = "howdy";
myGreeter.showGreeting();

class SpecialGreeter extends Greeter {
    constructor () {
        super("Very special greetings");
    }
}
path.join("Asdfa\sdf", "asdfsadfasdf\u2022");

`adfasdfa ${myGreeter}`;

let x = 1 + 6;
let y = 'asdf';
let z: boolean = false;
let a: Promise<string>;
let b = /[a-zA-z\d+]/g;

// Tests -----------------------------
console.log(123);

declare namespace GreetingLib {
    interface LogOptions {
        verbose?: boolean;
    }
    interface AlertOptions {
        modal: boolean;
        title?: string;
        color?: string;
    }
}
